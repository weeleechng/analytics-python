# database operations

import pyodbc
import db_credentials
# import logging

connection = None
dsn = None
usn = None
pwd = None
typ = None
sch = None
tbl = None


def db_setcredentials (key):
  if not key in db_credentials.odbc:
    print "Error! Unknown DSN " + key
    return False

  global dsn, usn, pwd, typ, sch, tbl
  dsn = db_credentials.odbc.get (key).get ("dsn")
  usn = db_credentials.odbc.get (key).get ("username")
  pwd = db_credentials.odbc.get (key).get ("password")
  typ = db_credentials.odbc.get (key).get ("type")
  sch = db_credentials.odbc.get (key).get ("schema")
  tbl = db_credentials.odbc.get (key).get ("table")

  return True
# /db_setcredentials


def db_connect (query_only = False):
  if dsn == None and usn == None and pwd == None:
    print "No database connection credentials!"
    return False

  global connection
  if query_only == False:
    print "Starting database connection"
  elif query_only == True:
    print "Starting database connection in READ-ONLY mode"
    #logging.basicConfig (filename='download_ga.log', level=logging.INFO)

  connection = pyodbc.connect ("DSN=" + dsn + ";UID=" + usn + ";PWD=" + pwd)
  print "Database connection established"
# /db_connect


def in_list (needle, haystack):
  if not isinstance (haystack, list):
    print "List type error"
    return False

  for item in haystack:
    if needle == item.lower ():
      return True
  
  return False
# /in_list


def db_column_type (fieldslist, fieldstype):
  print "Getting column types..."
  cur = connection.cursor ()
  for row in cur.columns (table=tbl, schema=sch):
    columname = str (row.column_name)
    if typ == "MSSQL" and columname in fieldslist and not fieldstype.has_key (columname):
      fieldstype[columname] = str (row.type_name)
    elif typ == "PostgreSQL" and in_list (columname, fieldslist) and not fieldstype.has_key (columname):
      fieldstype[columname] = str (row.type_name)

  return fieldstype
# /db_column_type


def debug_row_attribute (row):
  for attr in dir (row):
    if hasattr (row, attr):
      print ("row.%s = %s" % (attr, getattr(row, attr)))
# /debug_row_attribute


def db_column_size (fieldslist, fieldssize):
  print "Getting column sizes..."
  colsizequery = "SELECT column_name, character_maximum_length "
  colsizequery += "FROM information_schema.columns "
  colsizequery += "WHERE table_name = '" + tbl + "' "
  colsizequery += "AND column_name IN ("
  for field in fieldslist:
    colsizequery += "'" + field.lower () + "',"
  colsizequery = colsizequery[:-1]
  colsizequery += ")"
  
  colsizeresult = db_execute_read (colsizequery)
  for col in colsizeresult:
    fieldssize[col.column_name] = col.character_maximum_length

  return fieldssize
# /db_column_size


def db_execute_write (querystring, query_only = False):
  global connection
  if query_only == False:
    print "Executing write query... "
    cur = connection.cursor ()
    cur.execute (querystring)
    connection.commit ()
  else:
    print "Simulating write query execution..."
    print querystring
# /db_execute


def db_execute_read (querystring, query_only = False):
  if query_only == False:
    print "Executing select query..."
    global connection
    
    controlcount = 0
    recordkeeper = []
    status_flag = True # just because
    
    cur = connection.cursor ()
    cur.execute (querystring)
    while status_flag is True:
      controlcount += 1
      row = cur.fetchone ()

      if not row:
        status_flag = False
        break
      
      recordkeeper.append (row)

    # print str (controlcount) + " records retrieved"
    controlcount = 0

    return recordkeeper

  else:
    print "Simulating select query execution"
    #logging.info (querystring)
    return None
# /foreach_row


def db_close (query_only = False):
  global connection
  connection.close ()
  print "Database connection closed"
# /db_close
