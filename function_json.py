import json

def fromfile (fileObj):
  try:
    json_object = json.loads (fileObj.read ())
  except ValueError, e:
    print "JSON problem"
    return False
  except TypeError, e:
    print "Type problem"
    return False
  except AttributeError, e:
    print "Attribute problem"
    return False
  
  print "JSON file ok"
  return json_object
# /is_json_fileObj

def fromstring (string):
  try:
    json_object = json.loads (string)
  except ValueError, e:
    print "JSON problem"
    return False
  except TypeError, e:
    print "Type problem"
    return False
  except AttributeError, e:
    print "Attribute problem"
    return False
  
  print "JSON string ok"
  return json_object
# /is_json_fileObj
