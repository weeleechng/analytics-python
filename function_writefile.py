writefile_path = "download"

def writefile (filename, filecontent, filemode = "a", filepath = writefile_path):
  if filename == None:
    return False

  thefile = filepath + '/' + filename
  the_f = open (thefile, filemode)
  the_f.write (filecontent + '\n')
  the_f.close ()

# /writefile
