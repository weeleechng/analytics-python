import os
import sys
import getopt
import logging

import function_db
import function_json

connect_to = "Redshift"
query_only = False 


def check_arguments (argv):
  global connect_to, query_only

  helpstr = "Help? Just read the source code: cat add_goalx_columns.py"

  try:
    opts, args = getopt.getopt (argv, "hc:q", ["connect_to", "query_only"])
  except getopt.GetoptError:
    print helpstr
    sys.exit (2)

  for opt, arg in opts:
    if opt == '-h':
      print helpstr
      sys.exit ()

    elif opt in ("-c", "--connect_to"):
      connect_to = arg

    elif opt in ("-q", "--query_only"):
      query_only = True
# /check_arguments


def enclose_string (instr, enclosewith = '"'):
  instr = instr.encode ("ascii","ignore")
  return enclosewith + str (instr).replace (enclosewith, "") + enclosewith
# /enclose_string


def execute_update ():
  columndict = [
    {
      "columname" : "goal{{x}}Starts",
      "columntype" : "int"
    },
    {
      "columname" : "goal{{x}}Completions",
      "columntype" : "int"
    },
    {
      "columname" : "goal{{x}}Value",
      "columntype" : "decimal(13,2)"
    },
    {
      "columname" : "goal{{x}}ConversionRate",
      "columntype" : "float"
    },
    {
      "columname" : "goal{{x}}Abandons",
      "columntype" : "int"
    },
    {
      "columname" : "goal{{x}}AbandonRate",
      "columntype" : "float"
    },
    {
      "columname" : "searchGoal{{x}}ConversionRate",
      "columntype" : "float"
    }
  ]

  for x in range (2, 21):
    for column in columndict:
      alter_sql = "ALTER TABLE GoogleAnalytics "
      alter_sql += "ADD COLUMN "
      column_name = column.get ("columname")
      column_name = column_name.replace ("{{x}}", str (x))
      column_name = enclose_string (column_name, '"')
      alter_sql += column_name + " "
      alter_sql += column.get ("columntype") + " "
      alter_sql += "DEFAULT NULL"
      function_db.db_execute_write (alter_sql, query_only)
# /execute_update


def main ():
  logging.basicConfig (filename="add_goalx_columns.log", level=logging.INFO)

  print "ESTORM INTERNATIONAL - Amazon Redshift Alter Table tool"
  print "======================================================="
  
  check_arguments (sys.argv[1:])
  function_db.db_setcredentials (connect_to)
  function_db.db_connect ()
  execute_update ()
  function_db.db_close ()

# /main

main ()
