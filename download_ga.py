import os
import sys
import getopt
import datetime
import calendar
import logging
import time
import httplib2
import urllib2

import googleapiclient

import function_json
import function_db
import function_writefile

from apiclient.discovery import build
from oauth2client.client import SignedJwtAssertionCredentials

reportdatestart = None
reportdateend = None
reportsfolder = "reports"

keysfolder = "keys"

connect_to = "Redshift"
query_only = False 
simulate_only = False
download_only = False

ga_max_results = 10000
report_stagger = 250

recordfields = [
  "gaaccount",
  "gaproperty",
  "gapropertyname",
  "gaviewprofile",
  "gaid",
  "reportname",
  "date",
  "month",
  "year",
]

fieldslist = None # to be defined by get_fieldslist function
fieldstype = {} # to be populated by the db_column_type function
fieldssize = {} # to be populated by the db_column_size function

fieldstypetext = [
  "char",
  "text",
  "varchar",
  "nvarchar",
  "date",
  "datetime",
  "datetime2",
]

ga_file = None

now = datetime.datetime.now ()

def connect_to_analytics (keyfilename, email):
  if simulate_only == False:
    print "Initiating Google Analytics connection"
    
    f = file (keysfolder + "/" + str (keyfilename), 'rb')
    key = f.read ()
    f.close ()
    credentials = SignedJwtAssertionCredentials (email, key, scope='https://www.googleapis.com/auth/analytics.readonly')
    http = httplib2.Http ()
    http = credentials.authorize (http)

    return build ('analytics', 'v3', http=http)

  else:
    print "Simulating connection to Google Analytics"
    print " => Email: " + email
    print " => Key: " + keyfilename
    return None

# /connect_to_analytics


def organise_data (feed):
  data = []
  columnames = []
  for column in feed["columnHeaders"]:
    columnames.append (column.get("name").replace ("ga:", ""))

  for row in feed['rows']:
    datarow = {}
    columnindex = 0
    
    for value in row:
      datarow[columnames[columnindex]] = value
      columnindex += 1

    data.append (datarow)

  return data
# /organise_data


def fetch_data (reportname, service, view_id, dimensions, metrics, start_date, end_date, start_index, max_results = ga_max_results):
  if simulate_only == False:
    # Exponential backoff
    # https://developers.google.com/analytics/devguides/reporting/core/v3/coreErrors#backoff
    n = 1

    while True: # Retry loop
      print "Report: [" + reportname + "], start: [" + str (start_index) + "]"
      try:
        arguments = {
          'ids': view_id, # Your Google Analytics table ID goes here
          'dimensions': dimensions,
          'metrics': metrics,
          'start_date': start_date,
          'end_date': end_date,
          'start_index' : start_index,
          'max_results' : max_results,
        }

        data_query = service.data().ga().get(**arguments)
        feed = data_query.execute()

        # Reset retry counter
        if n > 0:
          n = 0

        break # Break free of while True

      except urllib2.HTTPError, error:
        print error.__class__, unicode(error)

        if error.resp.reason in ['userRateLimitExceeded', 'quotaExceeded']:
          sec = (2 ** n) + random.random()
          print "Rate limit exceeded, retrying in %ss" % sec
          time.sleep(sec)
          n += 1
        else:
          raise

      except googleapiclient.errors.HttpError, error:
        print "Error: " + str (error)
        logging.error ("Error: " + str (error))
        return None        

    if 'rows' not in feed:
      print ">> No results found for " + reportname
      logging.info (">> No results found for " + reportname)
      return

    data = organise_data (feed)
    return data

  else:
    print "Simulate fetching " + reportname
    print view_id
    print dimensions
    print metrics
    print start_date
    print end_date
    print start_index
    print max_results
    
    return None

# /fetch_data


def get_fieldslist (dimensions, metrics, additional_fields_array = recordfields):
  fieldslist = []
  fieldslist.extend (dimensions.replace("ga:", "").split (","))
  fieldslist.extend (metrics.replace("ga:", "").split (","))
  if isinstance (additional_fields_array, list):
    fieldslist.extend (additional_fields_array)

  return fieldslist
# /get_fieldslist


def get_fieldstype (fieldslist, fieldstype, additional_fields_array = recordfields):
  return function_db.db_column_type (fieldslist, fieldstype)
# /get_fieldstype


def get_fieldssize (fieldslist, fieldssize, additional_fields_array = recordfields):
  return function_db.db_column_size (fieldslist, fieldssize)
# /get_fieldstype


def init_query (headers):
  querystring = "INSERT INTO [" + function_db.sch + "].[" + function_db.tbl + "] ("
  for column in headers:
    querystring += '"' + column + '",'
  querystring = querystring[:-1]
  querystring += ")"
  querystring += " VALUES "

  return querystring
# /init_query


def cleanup_value (value):
  value = value.replace ("%", "")
  value = value.replace ("\\", "")
  value = value.replace ("'", "")
  value = value.replace ("--", "NULL")
  value = value.replace ("auto:", "")

  return value
# /cleanup_value


def build_and_execute_query (results):
  print "Rows retrieved: " + str (len (results))
  logging.info ("Rows retrieved: " + str (len (results)))
  if len (results) > 0:
    insertcount = 0
    totalcount = 0
    querystring = init_query (results[0].keys ())
    for row in results:
      querystring += "("

      for key, value in row.iteritems ():
        if function_db.typ == "MSSQL":
          value = value.encode ("utf-8")
        
        value = cleanup_value (value)
        
        if fieldstype[key.lower ()] not in fieldstypetext:
          querystring += value + ","
        elif function_db.typ == "MSSQL" and fieldstype[key.lower ()] == "nvarchar":
          querystring += "N'" + value + "',"
        else:
          column_size = fieldssize[key.lower ()]
          if len (value) > column_size:
            value = value[:-column_size]
          querystring += "'" + value + "',"

      querystring = querystring[:-1]
      querystring += "),"
      insertcount += 1
      totalcount += 1

      if insertcount == report_stagger:
        querystring = querystring[:-1]
        function_db.db_execute_write (querystring, query_only)
        #if query_only == True:
        #  logging.info (querystring)
        # start new insert query
        insertcount = 0
        querystring = init_query (results[0].keys())

    if insertcount > 0:
      querystring = querystring[:-1]
      function_db.db_execute_write (querystring, query_only)
      #if query_only == True:
      #  logging.info (querystring)

    print str (totalcount) + "/" + str (len (results)) + " rows inserted"
    logging.info (str (totalcount) + "/" + str (len (results)) + " rows inserted")

# /build_and_execute_query


def append_recordfields (results_holder, columnames, recordvalues):
  column_values = {}
  for i, column in enumerate (columnames):
    column_values[column] = recordvalues[i]

  for row in results_holder:
    row.update (column_values)

  return results_holder

# /append_recordfields


def write_results_to_file (download_filename, results_holder):
  print "Writing to " + download_filename
  logging.info ("Writing to " + download_filename)
  headerstring = ""
  for fieldname in results_holder[0].keys ():
    headerstring += '"' + fieldname + '",'
  headerstring = headerstring[:-1]

  function_writefile.writefile (download_filename, headerstring, "w")
  for row in results_holder:
    rowstring = ""
    for key, value in row.iteritems ():
      rowstring += '"' + value.encode ("utf-8") + '",'
    rowstring = rowstring[:-1]
    function_writefile.writefile (download_filename, rowstring)

  return True
# /write_results_to_file


def data_exists (fields, values):
  querystring = 'SELECT COUNT("rowId") as "rows"'
  querystring += " FROM " + function_db.tbl

  querystring += " WHERE "
  for columnindex in range (0, len (fields)):
    querystring += '"' + fields[columnindex] + '" = ' + "'" + values[columnindex] + "'"
    querystring += " AND "
  querystring = querystring[:-5]

  numrows = function_db.db_execute_read (querystring)
  numrows = numrows[0].rows
  
  if numrows == None or numrows == 0:
    return False
  else:
    return True

# /check_existing 


def process_file (filename):
  global fieldstype, fieldssize

  print " => " + filename
  logging.info (" => " + filename)
  
  reportdatestartstr = datetime_to_str (reportdatestart)
  reportdateendstr = datetime_to_str (reportdateend)
  print "Reporting date: " + reportdatestartstr + " to " + reportdateendstr
  logging.info ("Reporting date: " + reportdatestartstr + " to " + reportdateendstr)

  #if reportdateend.month < 10:
    #reportmonth = "0" + str (reportdateend.month)
  #else:
    #reportmonth = str (reportdateend.month)
  reportmonth = reportdateend.strftime ("%B")

  reportyear = str (reportdateend.year)

  jsonfile = open (filename, "r")
  jsondata = function_json.fromfile (jsonfile)
  
  if (jsondata != False):

    service = connect_to_analytics (jsondata["key_file"], jsondata["email_address"])

# The estorm Google Analytics JavaScript Object Notation structure:
# - email
# - key
# + reports
#   + report ID
#     - name
#     - dimensions
#     - metrics
# + accounts
#   + account name
#     + GA property ID
#       + GA property name
#       + Views
#         + view ID
#           - name
#           - date start
#           - date end
#           + reports
#             - report 1
#             - report 2
#             - report n

    for accountname, accountdetails in jsondata["accounts"].iteritems ():
      for accountpropid, accountproperty in accountdetails.iteritems ():
        #print "Account: " + accountname + " (" + accountpropid + ")"
        #print "Property: " + accountproperty["ga_property_name"]
        for viewid, viewproperties in accountproperty["views"].iteritems ():
          #print "View: " + viewproperties["ga_view"] + " (" + viewid + ")"
          reportstart = viewproperties["report_start"]
          reportend = viewproperties["report_end"]
          if reportend == "":
            reportend = reportdateendstr

          if reportdatestartstr >= reportstart and reportdateendstr <= reportend:
            print "Account: " + accountname + " (" + accountpropid + "), Property: " + accountproperty["ga_property_name"] + ", View: " + viewproperties["ga_view"] + " (" + viewid + ")"
            logging.info ("Account: " + accountname + " (" + accountpropid + "), Property: " + accountproperty["ga_property_name"] + ", View: " + viewproperties["ga_view"] + " (" + viewid + ")")
            for ga_report in viewproperties["reports"]:
              reportname = jsondata["reports"][ga_report]["report_name"]
              dimensions = jsondata["reports"][ga_report]["dimensions"]
              metrics = jsondata["reports"][ga_report]["metrics"]
              fieldslist = get_fieldslist (dimensions, metrics)
              fieldstype = get_fieldstype (fieldslist, fieldstype)
              fieldssize = get_fieldssize (fieldslist, fieldssize)

              recordvalues = [
                accountname,
                accountpropid,
                accountproperty["ga_property_name"],
                viewproperties["ga_view"],
                viewid,
                reportname,
                reportdateendstr,
                reportmonth,
                reportyear
              ]

              if query_only == False and data_exists (recordfields, recordvalues) == True:
                print ">> Data exists for " + viewproperties["ga_view"] + ", " + reportname
                logging.info (">> Data exists for " + viewproperties["ga_view"] + ", " + reportname)
                print " ------- "
                logging.info (" ------- ")
                continue

              download_filename = reportdateendstr.replace("-", "") + "_" + viewid.replace(":", "-") + "_" + viewproperties["ga_view"].replace(" ", "_") + "__" + reportname + ".csv"

              start_index = 1
              results_holder = []

              while True:
                results = fetch_data (reportname, service, viewid, dimensions, metrics, reportdatestartstr, reportdateendstr, start_index)
                if results != None and results != False:
                  for row in results:
                    results_holder.append (row)
                  if len (results) == ga_max_results:
                    start_index += ga_max_results
                  else:
                    break
                else:
                  break

              if len (results_holder) > 0:
                if query_only == False:
                  write_results_to_file (download_filename, results_holder)

                if download_only == False:
                  results_holder = append_recordfields (results_holder, recordfields, recordvalues)
                  build_and_execute_query (results_holder)

              print " ------- "
              logging.info (" ------- ")
          else:
            # print "Query start: " + reportdatestartstr + ", Report start: " + reportstart + ", " + str (reportdatestartstr >= reportstart)
            # print "Query end  : " + reportdateendstr + ", Report   end: " + reportend + ", " + str (reportdateendstr <= reportend)
            print ">>" + viewproperties["ga_view"]  + " (" + viewid + ") not within date range"
            logging.info (">>" + viewproperties["ga_view"]  + " (" + viewid + ") not within date range")
            print " ------- "
            logging.info (" ------- ")

  else:
    print "Halt: the JSON got problem"
    logging.info ("Halt: the JSON got problem")

  jsonfile.close ()
  print filename + " process complete"
  logging.info (filename + " process complete")
# /process_file


def scan_account_dir ():
  if len (os.listdir (reportsfolder)) > 0:
    filelist = []
    for file in os.listdir (reportsfolder):
      if (file.endswith (".json")):
        filelist.append (reportsfolder + "/" + file)
    return filelist
  else:
    return False
# /scan_account_dir


def robustify_filename (filename, filext = ".json"):
  return reportsfolder + "/" + filename.replace (reportsfolder + "/", "").replace (filext, "") + filext
# /robustify_filename


def check_and_process_files ():
  filelist = scan_account_dir ()
  if filelist != False and len (filelist) > 0:

    if ga_file != None and not ga_file in filelist:
      print "[x] Report configuration file <" + ga_file + "> not found"
      logging.info ("[x] Report configuration file <" + ga_file + "> not found")
      sys.exit ()

    if function_db.db_setcredentials (connect_to) == False:
      sys.exit ()

    function_db.db_connect (query_only)

    if ga_file == None:
      print "Found " + str (len (filelist)) + " files:"
      logging.info ("Found " + str (len (filelist)) + " files:")
      for filename in filelist:
        process_file (filename)
    else:
      process_file (ga_file)
    
    function_db.db_close (query_only)

  else:
    print "No report configuration .json files found in '" + accountfolder + "' folder."
    logging.info ("No report configuration .json files found in '" + accountfolder + "' folder.")
    print "Abort."
    sys.exit ()

# /check_and_process_files


def datetime_from_str (datestr, formatstr = "%Y-%m-%d"):
  # https://docs.python.org/2/library/datetime.html#strftime-and-strptime-behavior
  try:
    return datetime.datetime.strptime (datestr, formatstr)
  except ValueError, e:
    print "Halt: Input date " + datestr + " got problem"
    logging.info ("Halt: Input date " + datestr + " got problem")
    return None
# /datetime_from_str


def datetime_to_str (datetimeobj, formatstr = "%Y-%m-%d"):
  return datetimeobj.strftime (formatstr)
# /datetime_to_str


def debug ():
  print reportdatestart
  print reportdateend
  print connect_to
  print query_only
  print simulate_only

def check_arguments (argv):
  global reportdatestart, reportdateend, connect_to, ga_file, query_only, simulate_only, download_only

  helpstr = "Help? You want help? Just read the source code."

  inputday = ""
  inputmonth = ""
  inputyear = ""

  try:
    opts, args = getopt.getopt (argv, "hy:m:d:c:f:qsD", ["year=", "month=", "day=", "connect_to=", "filename=", "query_only", "simulate_only", "download_only"])
  except getopt.GetoptError:
    print helpstr
    sys.exit (2)

  for opt, arg in opts:
    if opt == '-h':
      print helpstr
      sys.exit ()

    elif opt in ("-y", "--year"):
      inputyear = arg

    elif opt in ("-m", "--month"):
      inputmonth = arg

    elif opt in ("-d", "--day"):
      inputday = arg

    elif opt in ("-c", "--connect_to"):
      connect_to = arg

    elif opt in ("-f", "--filename"):
      ga_file = robustify_filename (arg)

    elif opt in ("-q", "--query_only"):
      query_only = True

    elif opt in ("-s", "--simulate_only"):
      query_only = True
      simulate_only = True

    elif opt in ("-D", "--download_only"):
      download_only = True

  if not inputyear == "" or not inputmonth == "" or not inputday == "":
    if inputyear == "":
      inputyear = str (now.year)

    if inputmonth == "":
      inputmonth = str (now.month)

    if not inputday == "":
      reportdatestart = reportdateend = datetime_from_str (inputyear + "-" + inputmonth + "-" + inputday)

    if not inputmonth == "" and inputday == "":
      thismonthrange = calendar.monthrange (int (inputyear), int (inputmonth))
      reportdatestart = datetime_from_str (inputyear + "-" + inputmonth + "-1")
      reportdateend = datetime_from_str (inputyear + "-" + inputmonth + "-" + str (thismonthrange[1]))

  if reportdatestart == None and reportdateend == None:
    reportdatestart = reportdateend = now + datetime.timedelta (days=-1)

  # debug ()
# /check_arguments

def main ():
  logging.basicConfig (filename=function_writefile.writefile_path + '/download_ga.log', level=logging.INFO)

  print "estorm International - Google Analytics Report Download utility"
  
  print "Start: " + str (now)
  logging.info ("Start: " + str (now))
  
  check_arguments (sys.argv[1:])
  check_and_process_files ()

  timecomplete = datetime.datetime.now ()
  print "Complete: " + str (timecomplete)
  logging.info ("Complete: " + str (timecomplete))

main ()
